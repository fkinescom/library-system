package co.nescom;

import java.util.ArrayList;
import java.util.List;

class Library {
    private List<Book> books = new ArrayList<>();

    public void addBook(Book book) {
        books.add(book);
        System.out.println("Book added: " + book);
    }

    public void removeBook(Book book) {
        books.remove(book);
        System.out.println("Book removed: " + book);
    }

    public void displayBooks() {
        System.out.println("Library Collection:");
        for (Book book : books) {
            System.out.println(book);
        }
    }

    public Book[] getBooks() {
        return new Book[0];
    }
}