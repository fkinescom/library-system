package co.nescom;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Library library = new Library();

        while (true) {
            System.out.println("\nLibrary System Menu:");
            System.out.println("---------------------------------");
            System.out.println("1. Add a book");
            System.out.println("2. Remove a book");
            System.out.println("3. Display all books");
            System.out.println("---------------------------------");
            //
            System.out.println("4. Add Book Reader");  // dopisac kod
            System.out.println("5. Remove Book Reader");  // dopisac kod
            System.out.println("6. Display All Book Reader");  // dopisac kod
            System.out.println("---------------------------------");

            System.out.println("7. Borrowing a book");  // dopisac kod wypożyczenie książki
            System.out.println("8. Returning a borrowed book");  // dopisac kod wypożyczenie książki
            //    System.out.println("9. free option - what else can be useful");
            System.out.println("---------------------------------");
            //     System.out.println("10 test wielu liczb");
            System.out.println("---------------------------------");
            System.out.println("0. Exit");
            System.out.println();

            System.out.print("Select an option: ");
            int choice = scanner.nextInt();
            scanner.nextLine();  // Consume newline

            switch (choice) {
                case 1:
                    System.out.print("Enter title: ");
                    String title = scanner.nextLine();
                    System.out.print("Enter author: ");
                    String author = scanner.nextLine();
                    library.addBook(new Book(title, author));
                    break;
                case 2:
                    System.out.print("Enter title of the book to remove: ");
                    String removeTitle = scanner.nextLine();

                    // for do book serwis
                    for (Book book : library.getBooks()) {
                        if (book.getTitle().equalsIgnoreCase(removeTitle)) {
                            library.removeBook(book);
                            break;
                        }
                    }
                    break;
                case 3:
                    library.displayBooks();
                    break;
                case 4:
                    System.out.print("Enter Name: ");
                    String name = scanner.nextLine();
                    System.out.print("Enter Last Name: ");
                    String lastName = scanner.nextLine();
                    // bookReader.add          library.addB(new Book(title, author));
                    break;
                case 10:
                    System.out.println("test wielu liczb 10");
                    break;
                case 0:
                    scanner.close();
                    System.out.println("Exiting... by ");
                    System.exit(0);
                default:
                    System.out.println("Invalid option. Please choose a valid option.");
            }
        }
    }
}
