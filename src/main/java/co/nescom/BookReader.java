package co.nescom;

import java.util.ArrayList;
import java.util.List;

public class BookReader {
    private String name;
    private String lastName;

    public BookReader(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return "'" + name + "' " + lastName;
    }
}


// new book reader

/*

class newBookReader {
    private List<BookReader> bookRear = new ArrayList<>();

    public void addBook(Book book) {
        books.add(book);
        System.out.println("Book added: " + book);
    }

    public void removeBook(Book book) {
        books.remove(book);
        System.out.println("Book removed: " + book);
    }

    public void displayBooks() {
        System.out.println("Library Collection:");
        for (Book book : books) {
            System.out.println(book);
        }
    }

    public Book[] getBooks() {
        return new Book[0];
    }
}

*/